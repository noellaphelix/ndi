<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Http\Controllers\VehicleDetailController;
use Excel;

class ImportVehicles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $file_name; 
    public function __construct($data)
    {
        
        $this->file_name = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Excel::filter('chunk')->load(public_path().'/csv_vehicle/'.$this->file_name)->chunk(500, function($results) {

            $items = $results->toArray();

               foreach($items as $vehicle){
                
                $new = new Request();
                $new->query->add(["tofind" => str_replace(' ','',strtoupper(trim($vehicle['plate_no'])))]);

                 (new VehicleDetailController())->store($new);
               }
        });

    }
}
