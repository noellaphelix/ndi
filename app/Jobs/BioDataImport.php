<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\BioDataController;
use Illuminate\Http\Request;
use Excel;
use Log;

class BioDataImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
        protected $file_name; 


    public function __construct($data)
    {
        $this->file_name = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      Excel::filter('chunk')->load(public_path().'/csv_biodata/'.$this->file_name)->chunk(80, function($results) {

        $items = $results->toArray();

         foreach($items as $bio_data){
          
          $new = new Request();
          $new->query->add(["tofind" => str_replace(' ','',strtoupper(trim($bio_data['id'])))]);

           (new BioDataController())->store($new);
         }
      });
    }
}
