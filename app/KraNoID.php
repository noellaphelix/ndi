<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class KraNoID extends Model
{
    
    protected $fillable = ['kra','user_id'];
}
