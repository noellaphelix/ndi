<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BioData extends Model
{
    
    protected $fillable = ['ID_NUMBER','SERIAL_NUMBER','DATE_OF_ISSUE','FULL_NAMES','GENDER','DATE_OF_BIRTH','DISTRICT_OF_BIRTH','FATHER_NAMES','MOTHER_NAMES','ADDRESS'];

   
    public function kra()
    {
		return $this->hasOne(KraPin::class,'bio_data_id','id');
    }

    public function dl()
    {
    	return $this->hasOne(DrivingLicence::class);

    }
}
