<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KraPin extends Model
{
    
   	protected $fillable =['bio_data_id','TAXPAYERPIN','LASTNAME','FIRSTNAME','MIDDLENAME','MPOBOX','MTOWN','POCODE','RTELNO','IDTYPE','IDNUMBER','STARTUPDATE'];

    public function biodata()
    {
    	return $this->belongsTo(BioData::class,'bio_data_id','id');
    }
    
    public function vehicles()
    {
    	return $this->belongsToMany(VehicleDetail::class,'karpin_vehicledetail')->withTimestamps();
    }



}
