<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleDetail extends Model
{
    
    protected $fillable = ['chassisNumber','yearOfManufacture','carMake','carModel','regNo','bodyType','logbookNumber','registrationDate','bodyColor','dutyStatus','dutyAmount','dutyDate','engineNumber','owner_id','caveat'];

    public function krapin()
    {
    	return $this->belongsToMany(KraPin::class,'karpin_vehicledetail')->withTimestamps();
    }

}
