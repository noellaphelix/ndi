<?php

namespace App\Ad;

use App\AdConfig;
use App\User;
use Carbon\Carbon;
use Auth;
/**
 * 
 */
class AdLogin implements AdInterface
{

	protected $connection;

	protected $ldap_connection;
 
	protected $ldap_base_dn = 'DC=domain,DC=tld,DC=tld';

	public function makeconnection()
	{
		$config_data = AdConfig::first();

			if(! \ldap_connect($config_data->server)){
				dd("could Not connect");
			}
		$this->ldap_connection = \ldap_connect($config_data->server);

	}

	public function attempt($username,$password)
	{	
		$this->makeconnection();

		if($this->loginFromApp($username,$password)){
			return ;
		}

		\ldap_set_option($this->ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
		\ldap_set_option($this->ldap_connection, LDAP_OPT_REFERRALS, 0);
		try{

		$data = \ldap_bind($this->ldap_connection, $username, $password);

		}catch(\Exception $e){

		}
		
		return isset($data) ? $this->localverification($data,$username) : false;	

	}

	public function localverification($data,$username)
	{

			//Your domains DN to query
		    $ldap_base_dn = 'DC=JUBILEEKENYA,DC=COM';
			
			//Get standard users and contacts
		   // $search_filter = '(|(objectCategory=person)(objectCategory=contact))';
			//$search_filter ="(sAMAccountName=".$username.")";
			$search_filter ="(mail=".$username.")";

			//$pattern = "/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,})$/";

			//Connect to LDAP
			$result = \ldap_search($this->ldap_connection, $ldap_base_dn, $search_filter, array('dn','userprincipalname'),0,0);
			
		    if (FALSE !== $result){

				$entries = \ldap_get_entries($this->ldap_connection, $result);

				if(isset($entries['count']) && $entries['count'] == 0){

					return false;

				}else{

					$department_array = explode(',', $entries[0]['dn']);
					
					$user = User::where('email',$username)->orWhere('username',$username)->first();
					if($user){

						if(!$user->status){
							return ;
						}else{

						Auth::Login($user);
						$now  = Carbon::now();
						$user->update(['last_login' => $now]);

						return ;
						}

					}else{
					$user = User::create([
						'email' => $entries[0]['userprincipalname'][0],
						'username' => $username,
						'password' => bcrypt("secret"),
						'name' =>  $entries[0]['userprincipalname'][0],
						'department' => $department_array[1],
						'allowed' => false,
						'status' => User::ACTIVE,
					])->id;

					Auth::login(User::find($user));
				}
			}
		}

		return ;
	}

	public function loginFromApp($username,$password)
	{

		$local = User::where('email',$username)->where('status',User::ACTIVE)->first();

		if(count($local) > 0){

				if($local->allowed){
					if(Auth::attempt(['email'=>$username,'password'=>$password])){
						return true;
					}
					return false;
				}
					return false;
				
		}

			return false;


	}
	
}