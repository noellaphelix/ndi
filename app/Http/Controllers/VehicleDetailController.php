<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehicleDetail;
use App\AdaApiRepo\ImportVehicleData;
use App\Jobs\ImportVehicles;
use Carbon\Carbon;

class VehicleDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vdetails = VehicleDetail::take(10)->get();

        return view('vehicle-detail.index',['vdetails' => $vdetails]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $data = $request->validate([
            'tofind' =>'required'
        ]);

        $findArray = collect(explode(',', $data['tofind']))->filter();


        $availabeData = VehicleDetail::whereIn('regNo',$findArray->toArray())->get();

        if($availabeData->count() > 0){

            $findArray = $this->runfilter($findArray, $availabeData);

        }

        if($findArray->count() == 0){

            return redirect()->back()->with(['data' => $availabeData]);

        }else{

             $searchResults = Collect(ImportVehicleData::import($findArray));

             return redirect()->back()->with(['data' => $availabeData,'searchResults' => $searchResults]);
        }


        return ;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = VehicleDetail::with(['krapin.biodata'])->find($id);

        return view('vehicle-detail.show',['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
        public function importFromADA()
    {

        return view('vehicle-detail.import');
    }

    public function runfilter($findArray , $availabeData)
    {
        $nda_getData = $findArray->reject(function($needle) use($availabeData){   

             $data =$availabeData->map(function($available) use ($needle){

                if($available->regNo == $needle)
                {
                    return true;
                }
                return false;
            }); 

           return $data[0];

        });
        return $nda_getData;
    }

    public function importCSV(Request $request)
    {
        if($request->hasFile('file')){

            $csvfile = $request->file('file');

            $file_name = Carbon::now()->format('Ymdhis').'.'.$csvfile->getClientOriginalExtension();

            $destinationPath = public_path('/csv_vehicle');
            $csvfile->move($destinationPath,$file_name);

           ImportVehicles::dispatch($file_name)->delay(now()->addMinutes(1)); 

           return redirect()->back()->with('message','We are processing your Request , please Refresh the list after A minute');

        }else{

            return redirect()->back()->with('error' ,'Please attach CSV file ');
        }
    }
}
