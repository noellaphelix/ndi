<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VehicleDetail;
use App\AdaApiRepo\ImportVehicleData;
use App\Http\Resources\VehicleDetails;


class VehicleDetailsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return VehicleDetails::collection(VehicleDetail::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $data = $request->validate([
            'tofind' =>'required'
        ]);

        $findArray = collect(explode(',', $data['tofind']))->filter();


        $availabeData = VehicleDetail::whereIn('regNo',$findArray->toArray())->get();


        if($availabeData->count() > 0){

            $findArray = $this->runfilter($findArray, $availabeData);
        }

        if($findArray->count() == 0){

            return redirect()->back()->with(['data' => $availabeData]);

        }else{

             $searchResults = Collect(ImportVehicleData::import($findArray));

             return redirect()->back()->with(['data' => $availabeData,'searchResults' => $searchResults]);
        }


        return ;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        return new VehicleDetails(VehicleDetail::where('regNo',$id)->first());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
        public function importFromADA()
    {

        return view('vehicle-detail.import');
    }

    public function runfilter($findArray , $availabeData)
    {
        $nda_getData = $findArray->reject(function($needle) use($availabeData){   

             $data =$availabeData->map(function($available) use ($needle){

                if($available->regNo == $needle)
                {
                    return true;
                }
                return false;
            }); 

           return $data[0];

        });
        return $nda_getData;
    }
}
