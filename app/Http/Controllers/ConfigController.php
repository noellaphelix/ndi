<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Config;
use GuzzleHttp\Client;


class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
            $config =  Config::first();

        return view('admin.config.index',['config' => $config]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'grant_type' => 'required',
            'password' => 'required',
            'client_id' => 'required',
            'client_secret' => 'required',
            'username' => 'required'
            ]);


        Config::create($data);

        return redirect()->route('config.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Config $config)
    {
        $data = $request->validate([
            'grant_type' => 'required',
            'password' => 'required',
            'client_id' => 'required',
            'client_secret' => 'required',
            'username' => 'required'
            ]);

        $config->update($data);

        return redirect()->route('config.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function refreshToken()
    {
        $config = Config::select('grant_type','username','password','client_id','client_secret')->first()->toArray();

        $client = new \GuzzleHttp\Client();
        $response = $client->post(
            'https://api.ndi.co.ke/oauth/token',
            [
                \GuzzleHttp\RequestOptions::JSON => $config
            ],
            ['Content-Type' => 'application/json']
            );

        $responseJSON = json_decode($response->getBody(), true);

        $updateToken = Config::find(1)->update(['token' => $responseJSON['access_token']]);

        return redirect()->route('config.index');
    }
}
