<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Ad\AdInterface;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $adinterface;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AdInterface $adinterface)
    {
        $this->adinterface = $adinterface;
        $this->middleware('guest')->except('logout');

    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    public function redirectTo()
    {
        return redirect()->route('un-list.index');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    
    public function attemptLogin(Request $request)
    {
        $this->adinterface->attempt($request->email.'@jubileekenya.com', $request->password);
    }

    public function attempt($username, $passowrd)
    {

        return;
    }

}
