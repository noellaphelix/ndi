<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BioDataApiController extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_no' => $this->ID_NUMBER,
            'serial_number' => $this->SERIAL_NUMBER,
            'date_of_issue' => $this->DATE_OF_ISSUE,
            'names' => $this->FULL_NAMES,
            'gender' => $this->GENDER,
            'dob' => $this->DATE_OF_BIRTH,
            'distric_of_birth' => $this->DISTRICT_OF_BIRTH,
            'father_names' => $this->FATHER_NAMES,
            'mother_names' => $this->MOTHER_NAMES,
            'address' => $this->ADDRESS,
        ];
    }
}
