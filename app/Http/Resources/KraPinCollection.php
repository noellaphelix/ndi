<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KraPinCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
           
            'kra_pin' => $this->TAXPAYERPIN,
            'l_name' => $this->LASTNAME,
            'f_name' => $this->FIRSTNAME,
            'm_name' => $this->MIDDLENAME,
            'address' => $this->MPOBOX,
            'town' => $this->MTOWN,
            'pcode' => $this->POCODE,
            'tel' => $this->RTELNO,
            'id_type' => $this->IDTYPE,
            'id_no' => $this->IDNUMBER,
            'start_data' => $this->STARTUPDATE,
            'updated_at' => $this->create_at,
        ];
    }
}
