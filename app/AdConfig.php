<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdConfig extends Model
{
     protected $fillable =['ad_enabled','server','port','user_dn','bind_attribute','login_attribute','authenication','username','password'];
}
