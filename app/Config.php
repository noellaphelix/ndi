<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    
    protected $fillable = ['grant_type','password','client_id','client_secret','username','token'];
    
}
