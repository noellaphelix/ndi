<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrivingLicence extends Model
{
    

    protected $fillable = ['bio_data_id','DLNO','NAMES','IDNO','PIN','VALIDDATE','DLCLASS','SMARTDL'];


    public function biodata()
    {
    	return $this->belongsTo(BioData::class);
    }
}
