<?php

namespace App\AdaApiRepo;
use GuzzleHttp\Client;
use Auth;
use App\UnfoundIdno;
use App\Config;
use App\BioData;
use App\NotFoundError;
use Log;

/**
 * Let use this to get bio data
 */
class ImportBioData 
{
	
	protected static $URL="";
	protected static $TOKEN="";


	public  static function prepare()
	{
		self::$TOKEN = Config::first()->token;
		self::$URL = 'https://api.ndi.co.ke/api/v1/get/1/integrations_accessid,id/SYS-20170222144241005';

	}

	public static function  import($findArray)
	{
			$data['not_found'] = [];
			$data['found'] = [];

			self::prepare();
			
			foreach($findArray as $request){

				$response = json_decode(self::makeRequest($request));

				if(isset($response->Status) && (($response->Status == "Null") || ($response->Status == 'Failed'))){
					array_push($data['not_found'],$request);
						UnfoundIdno::create([
							'id_no' =>$request,
							'user_id' => Auth::id()??'0',
					]);
				}else{

					self::storeResponse($response);
					array_push($data['found'],$response);

				}
			}

		return $data;
	}

	public static function makeRequest($request)
	{	
			$request = str_replace('.','',$request);
			try{
			$client = new \GuzzleHttp\Client(['verify' => false ]);
			$res = $client->request('GET', self::$URL.','.str_replace(' ','',$request),[
				'headers' =>[
					'Authorization' => 'Bearer '.self::$TOKEN,
					'Accept' => 'application/json',
					'http_errors' => false,
				]
			]);

			$response = $res->getBody()->getContents();
		}catch(\Exeception $e){
			Log::info(print_r($e,true));
		}

			return $response; 
	}

	public  static function storeResponse($response)
	{

		foreach($response as $data){
			BioData::create(collect($data)->toArray());

		}
		return;
	}
}