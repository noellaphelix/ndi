<?php

namespace App\AdaApiRepo;
use GuzzleHttp\Client;

use App\Config;
use App\BioData;
use App\DrivingLicence;
/**
 * Let use this to get bio data
 */
class ImportDlData 
{
	
	protected static $URL="";
	protected static $TOKEN="";


	public  static function prepare()
	{
		self::$TOKEN = Config::first()->token;
		self::$URL = 'https://api.ndi.co.ke/api/v1/get/4/integrations_accessid,id/SYS-20170222144241005';

	}

	public static function  import($findArray)
	{
		$data['not_found'] = [];
		$data['found'] = [];
		self::prepare();
		
		foreach($findArray as $request){

			$response = json_decode(self::makeRequest($request));

			if(isset($response->Status) &&  ($response->Status == "Null" || $response->Status =='Failed' )){
				array_push($data['not_found'],$request);
			}else{

				self::storeResponse($response);
				array_push($data['found'],$response[0]);

			}
		}

		return $data;
	}

	public static function makeRequest($request)
	{	
			$client = new \GuzzleHttp\Client();
			$res = $client->request('GET', self::$URL.','.$request,[
				'headers' =>[
					'Authorization' => 'Bearer '.self::$TOKEN,
					'Accept' => 'application/json'
				]
			]);

			$response = $res->getBody()->getContents();

			return $response; 
	}

	public  static function storeResponse($response)
	{
		foreach($response as $data){
			
			$biodata =  BioData::where('ID_NUMBER',$data->IDNO)->first();
			if(count($biodata)> 0){
				$data->bio_data_id = $biodata->id;
			}else{
				$findArray = collect(explode(',',$data->IDNO))->filter();
				$import = ImportBioData::import($findArray);

				if(count($import['found']) > 0){

					$biodata =  BioData::where('ID_NUMBER',$data->IDNO)->first();

					$data->bio_data_id = $biodata->id;

				}else{

				$data->bio_data_id = null;

				}
			}
			
			DrivingLicence::create(collect($data)->toArray());

		}

		return;
	}
}