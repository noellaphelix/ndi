<?php

namespace App\AdaApiRepo;


use GuzzleHttp\Client;
use App\Config;
use App\KraPin;
use App\BioData;
use App\AdaApiRepo\ImportBioData;
use App\UnfoundKra;
use App\User;
use App\KraNoID;
use Auth;
use App\NotFoundError;
use Log;
/**
 * Let use this to get bio data
 */
class ImportKraData 
{
	
	protected static $URL="";
	protected static $TOKEN="";


	public  static function prepare()
	{
		self::$TOKEN = Config::first()->token;
		self::$URL = 'https://api.ndi.co.ke/api/v1/get/3/integrations_accessid,pin/SYS-20170222144241005';

	}

	public static function  import($findArray)
	{
			$data['not_found'] = [];
			$data['found'] = [];
			self::prepare();
			
			foreach($findArray as $request){

				$response = json_decode(self::makeRequest($request));

				if(isset($response->Status) &&  (($response->Status == "Null") || ($response->Status == 'Failed'))){

					UnfoundKra::create([
							'kra' =>$request,
							'user_id' => Auth::id()??'0',
					]);

					array_push($data['not_found'],$request);

				}else{

					$id = self::storeResponse($response , $request);
					$response[0]->kra_id = $id;
					array_push($data['found'],$response);

				}
			}
		return $data;
	}

	public static function makeRequest($request)
	{	
			$client = new \GuzzleHttp\Client(['verify' => false ]);
			$res = $client->request('GET', self::$URL.','.str_replace(' ','',$request),[
				'headers' =>[
					'Authorization' => 'Bearer '.self::$TOKEN,
					'Accept' => 'application/json'
				]
			]);

			$response = $res->getBody()->getContents();


			return $response; 
	}

	public  static function storeResponse($response ,$request=null)
	{
		

		foreach($response as $data){

			if(isset($data->APPELLATION)){

				$id = self::registerCompany($data);// bio data

				return self::createCampany($data,$id); //return kra_pin_id

			}else{

				if(!self::isValidId($data->IDNUMBER,$request)){
					$data->bio_data_id = null;
				}else{

					$data->bio_data_id = self::getBioData($data->IDNUMBER,$request);

					}
					KraPin::create(collect($data)->toArray());
				}
			}

		return;
	}

	public static function registerCompany($data)
	{

		$bData = new BioData();
		$bData->ID_NUMBER = $data->IDNUMBER;
		$bData->SERIAL_NUMBER = $data->EMPLOYERSPIN;
		$bData->ADDRESS = $data->POCODE;
		$bData->FULL_NAMES = $data->APPELLATION;
		$bData->DISTRICT_OF_BIRTH= $data->MCTOWN;
		$bData->GENDER = "COMPANY";
		$bData->DATE_OF_ISSUE = $data->STARTUPDATE;

		$bData->save();

		return $bData->id;
	}

	public static function createCampany($data,$id): int
	{

		$kra = new KraPin();
		$kra->bio_data_id = $id;
		$kra->TAXPAYERPIN = $data->EMPLOYERSPIN;
		$kra->MIDDLENAME = $data->APPELLATION;
		$kra->MPOBOX = $data->MCPOBOX;
		$kra->MTOWN = $data->MCTOWN;
		$kra->POCODE = $data->POCODE;
		$kra->RTELNO = $data->PHCTELNO;
		$kra->IDTYPE = $data->IDTYPE;
		$kra->IDNUMBER = $data->IDNUMBER;
		$kra->STARTUPDATE = $data->STARTUPDATE;
		$kra->save();

		return $kra->id;
	}

	public static function isValidId($idnumber,$request)
	{
		if(preg_match('/[a-zA-Z\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/',$idnumber)){
			NotFoundError::create([
						'search_type' => 'kra pin search',
						'search_string' => $request,
						'endpoint' => self::$URL,
						'request' => $idnumber,
					]);

			return false;

		}else{

			return true;
		}

	}

	public static function getBioData($idnumber, $request = null)
	{				
					$idnumber = (int) $idnumber;
					$bio_data_id = null;
					$biodata =  BioData::where('ID_NUMBER',$idnumber)->first();

					if(count($biodata)> 0){

						$bio_data_id = $biodata->id;

					}else{
						$findArray = collect(explode(',',$idnumber))->filter();

						$import = ImportBioData::import($findArray);

						if(count($import['found']) > 0){

							$biodata =  BioData::where('ID_NUMBER',$idnumber)->first();

							if(count($biodata) > 0){

								$bio_data_id = $biodata->id;

							}else{

									KraNoID::create([
										'user_id' => Auth::id()??0,
										'kra' =>$request,
									]);

								$bio_data_id  = null;
							}

						}
					}

					return $bio_data_id;
						
	}


}