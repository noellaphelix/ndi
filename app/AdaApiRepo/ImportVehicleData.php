<?php

namespace App\AdaApiRepo;
use GuzzleHttp\Client;

use App\Config;
use App\VehicleDetail;
use App\KraPin;
use App\BioData;
use Log;
/**
 * Let use this to get bio data
 */
class ImportVehicleData 
{
	
	protected static $URL="";
	protected static $TOKEN="";

	protected static $tosearch;

	public  static function prepare()
	{
		self::$TOKEN = Config::first()->token;
		self::$URL = 'https://api.ndi.co.ke/api/v1/get/2/integrations_accessid,regno/SYS-20170222144241005';

	}

	public static function  import($findArray)
	{
			$data['not_found'] = [];
			$data['found'] = [];
			self::prepare();
			
			foreach($findArray as $request){

				$response = json_decode(self::makeRequest($request));

				if(isset($response->Status) &&  (($response->Status == "Null") || ($response->Status == 'Failed'))){
					array_push($data['not_found'],$request);
				}else{
					self::storeResponse($response);
					array_push($data['found'],$response);

				}
			}

		return $data;
	}

	public static function makeRequest($request)
	{		
		self::$tosearch = $request;
			$client = new \GuzzleHttp\Client();
			$res = $client->request('GET', self::$URL.','.str_replace(' ','',$request),[
				'headers' =>[
					'Authorization' => 'Bearer '.self::$TOKEN,
					'Accept' => 'application/json'
				]
			]);

			$response = $res->getBody()->getContents();

			return $response; 
	}

	public  static function storeResponse($response)
	{
			
		$data = $response;
		$vehicle = new VehicleDetail();
		$vehicle ->chassisNumber = $data->chassisNumber;
		$vehicle ->yearOfManufacture = $data->vehicle->yearOfManufacture;
		$vehicle ->carMake = $data->vehicle->carMake;
		$vehicle ->carModel = $data->vehicle->carModel;
		$vehicle ->regNo = $data->vehicle->regNo;
		$vehicle ->bodyType = $data->vehicle->bodyType;
		$vehicle ->logbookNumber = $data->vehicle->logbookNumber;
		$vehicle ->registrationDate = $data->vehicle->registrationDate;
		$vehicle ->bodyColor = $data->vehicle->bodyColor;
		$vehicle ->dutyStatus = $data->vehicle->dutyStatus;
		$vehicle ->dutyAmount = $data->vehicle->dutyAmount;
		$vehicle ->dutyDate = $data->vehicle->dutyDate;
		$vehicle ->engineNumber = $data->vehicle->engineNumber;
		$vehicle ->caveat = $data->caveat;


		$owners = [];

		if(count((object)$data->owner)> 0){

			foreach((object)$data->owner as $value) {

				$result = self::findOwner($value->PIN);
					if(!$result){
						return;
					}else{
						
						array_push($owners,$result);
					}
			}
		}

		$vehicle->save();

		self::createRelationShip($vehicle->id,$owners);
		return;
	}


	public static function findOwner($pin)
	{
		$owner = KraPin::where('TAXPAYERPIN',$pin)->with(['biodata'])->first();
		
		if(count($owner) > 0){
			return  $owner->biodata->id;
		}else{

			$data  = ImportKraData::import(collect($pin));

			if(count($data['found']) >0){

			$data = collect($data['found'])->flatten();

			$id= null;
				foreach($data as $value){

					$id = $value->kra_id ?? self::getBioData($value->IDNUMBER);
				}
				return $id;
			}else{
				return false;
			}
		}

	}

	public static function getBioData($id)
	{
		$owner = BioData::where('ID_NUMBER',$id)->first();
		if(count($owner) >0){

			return $owner->id;
		}else{

			$import = ImportBioData::import(collect($id));
			$data = collect($import['found']);
			return  self::getBioData($data[0]->ID_NUMBER); 
		}
	}

	public static function createRelationShip($id,$owners)
	{
		$vehicle = VehicleDetail::find($id);

		$vehicle->krapin()->sync($owners);

		return ;
	}
}