<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotFoundError extends Model
{
    

    protected $fillable = ['search_type','endpoint','request','search_string'];
}
