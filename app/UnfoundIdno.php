<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnfoundIdno extends Model
{
     protected $fillable = ['id_no','user_id'];
}
