<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnfoundKra extends Model
{
    protected $fillable = ['kra','user_id'];
}
