<?php

use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        App\AdConfig::create([
        	'ad_enabled' => 1,
        	'server' => 'ldap://mail.jubileekenya.com',
        	'port' => 389,
        	'user_dn' => 'OU=Domain Users,DC=jubileekenya,DC=com',
        	'bind_attribute' => 'userPrincipleName',
        	'login_attribute' => 'sAMAccountName',
        	'authenication' => 1,
        	'username'=>'kevin.njenga@jubileekenya.com',
        	'password'=> 'test'
        ]);
    }
}
