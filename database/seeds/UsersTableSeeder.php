<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name'=> 'admin',
            'username'=> 'aml.Admin',
            'email' => 'admin@jubileekenya.com',
            'status' => App\User::ACTIVE,
            'role_id' => App\User::ADMIN,
            'password' => bcrypt('password'),
            'allowed' => App\User::ALLOWED,
        ]);
    }
}
