<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ad_enabled');
            $table->string('server');
            $table->integer('port');
            $table->string('user_dn');
            $table->string('bind_attribute');
            $table->string('login_attribute');
            $table->integer('authenication');
            $table->string('username');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_configs');
    }
}
