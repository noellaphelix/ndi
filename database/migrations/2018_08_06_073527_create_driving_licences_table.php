<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrivingLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driving_licences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bio_data_id')->nullable();
            $table->string('DLNO');
            $table->string('NAMES');
            $table->string('IDNO')->nullable();
            $table->string('PIN')->nullable();
            $table->string('VALIDDATE');
            $table->string('DLCLASS');
            $table->string('SMARTDL');
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driving_licences');
    }
}
