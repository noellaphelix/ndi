<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKraPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kra_pins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bio_data_id')->nullable();
            $table->string('TAXPAYERPIN');
            $table->string('LASTNAME')->nullable();
            $table->string('FIRSTNAME')->nullable();
            $table->string('MIDDLENAME')->nullable();
            $table->string('MPOBOX')->nullable();
            $table->string('MTOWN')->nullable();
            $table->string('POCODE')->nullable();
            $table->string('RTELNO')->nullable();
            $table->string('IDTYPE')->nullable();
            $table->string('IDNUMBER')->nullable();
            $table->string('STARTUPDATE')->nullable();
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kra_pins');
    }
}
