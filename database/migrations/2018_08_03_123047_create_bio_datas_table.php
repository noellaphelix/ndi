<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBioDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bio_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ID_NUMBER')->nullable();
            $table->string('SERIAL_NUMBER')->nullable();
            $table->string('DATE_OF_ISSUE')->nullable();
            $table->string('FULL_NAMES');
            $table->string('GENDER');
            $table->string('DATE_OF_BIRTH')->nullable();
            $table->string('DISTRICT_OF_BIRTH')->nullable();
            $table->string('FATHER_NAMES')->nullable();
            $table->string('MOTHER_NAMES')->nullable();
            $table->string('ADDRESS')->nullable();
            $table->timestamps();

          //   $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
          // $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bio_datas');
    }
}
