<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chassisNumber');
            $table->string('owner_names_list')->nullable();
            $table->string('yearOfManufacture')->nullable();
            $table->string('carMake')->nullable();
            $table->string('carModel')->nullable();
            $table->string('regNo');
            $table->string('bodyType')->nullable();
            $table->string('logbookNumber')->nullable();
            $table->string('registrationDate')->nullable();
            $table->string('bodyColor')->nullable();
            $table->string('dutyStatus')->nullable();
            $table->string('dutyAmount')->nullable();
            $table->string('dutyDate')->nullable();
            $table->string('engineNumber')->nullable();
            $table->string('owner_id')->nullable();
            $table->string('caveat')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_details');
    }
}
