<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('logout',function(){
	Auth::logout();
	return redirect()->route('login');
});

Route::group(['middleware' =>['auth']], function(){

		Route::get('dashboard','HomeController@dashboard');
		Route::get('bio-data/import','BioDataController@importFromADA');
		Route::get('dl/import','DlController@importFromADA');
		Route::get('kra-pin/import','KraPinController@importFromADA');
		Route::get('vehicle-detial/import','VehicleDetailController@importFromADA');
		Route::get('config/token-refresh' , 'ConfigController@refreshToken');

		Route::post('vehicle_csv','VehicleDetailController@importCSV');
		Route::post('bio_data_csv','BioDataController@importCSV');
		Route::post('kar_csv_import','KraPinController@importCSV');
		//resources
		Route::resource('config' , 'ConfigController');
		Route::resource('bio-data','BioDataController');
		Route::resource('dl','DlController');
		Route::resource('kra-pin','KraPinController');
		Route::resource('vehicle-detial','VehicleDetailController');

});