@extends('layouts.maincontent')
@section('content')
<div class="row">
	<div class="col-md-6 ">
		<form method="POST" action="{{ url('kra-pin') }}">
			@csrf
			<div class="form-group">
				<label>Enter KRA PIN  to Verify</label>
				<textarea name="tofind" class="form-control" rows="4"></textarea>
			</div>
			<div class="form-group">
				<button  type="submit" class="btn btn-info btn-block fa fa-send"> get</button>
			</div>
		</form>
	</div>
	<div class="col-md-6">
		@if(session('searchResults'))
		@php($notfound = session('searchResults'))
	    <div class="panel panel-bd lobidrag">
	        <div class="panel-heading">
	            <div class="panel-title">
	                <h4 class="text-danger">No Result Found For</h4>
	            </div>
            </div>
           <div class="panel-body">
			@foreach($notfound['not_found'] as $key => $value)
				<span class="label label-warning">{{ $value }}</span>
				
			@endforeach
			</div>
		</div>
		@endif
	</div>

</div>

<div class="row">
		    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>KRA PIN Data</h4>
                </div>
            </div>
            <div class="panel-body">
                <p class="m-b-15">Found Results
                	<a href="{{ url('bio-data/import')}}" class="btn btn-sm btn-success fa fa-search pull-right"> Search </a>
                </p>
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
   						<thead>
							<th>Kra Pin</th>
							<th>L. Name</th>
							<th>F. Name</th>
							<th>M. Name</th>
							<th>M P.O BOX</th>
							<th>Town</th>
							<th>P Code</th>
							<th>R Tel No</th>
							<th>Id Type</th>
							<th>ID no</th>
							<th>STARTUPDATE</th>
						</thead>
						<tbody>
						@if(session('data'))
							@php($data = session('data'))
							@foreach($data as $pin)
						<tr>
							<td>{{ $pin->TAXPAYERPIN }} </td>
							<td>{{ $pin->LASTNAME }} </td>
							<td>{{ $pin->FIRSTNAME }} </td>
							<td>{{ $pin->MIDDLENAME }} </td>
							<td>{{ $pin->MPOBOX }} </td>
							<td>{{ $pin->MTOWN }} </td>
							<td>{{ $pin->POCODE }} </td>
							<td>{{ $pin->RTELNO }} </td>
							<td>{{ $pin->IDTYPE }} </td>
							<td>{{ $pin->IDNUMBER }} </td>
							<td>{{ $pin->STARTUPDATE }} </td>
						</tr>
							@endforeach
						@endif

						@if(session('searchResults'))
							@php($data = session('searchResults'))
							@foreach($data['found'] as $key => $pin)
						<tr>
							<td>{{ $pin[$key]->TAXPAYERPIN }} </td>
							<td>{{ $pin[$key]->LASTNAME }} </td>
							<td>{{ $pin[$key]->FIRSTNAME }} </td>
							<td>{{ $pin[$key]->MIDDLENAME }} </td>
							<td>{{ $pin[$key]->MPOBOX }} </td>
							<td>{{ $pin[$key]->MTOWN }} </td>
							<td>{{ $pin[$key]->POCODE }} </td>
							<td>{{ $pin[$key]->RTELNO }} </td>
							<td>{{ $pin[$key]->IDTYPE }} </td>
							<td>{{ $pin[$key]->IDNUMBER }} </td>
							<td>{{ $pin[$key]->STARTUPDATE }} </td>
						</tr>
							@endforeach
						@endif

						</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection