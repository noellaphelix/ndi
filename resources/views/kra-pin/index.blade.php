@extends('layouts.maincontent')
@section('content')
<div class="row">
	<div class="col-md-12">
		@if(session('message'))
		<div class="alert alert-success">
			{{session('message')}}
		</div>
		@endif
	</div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>KRA Details</h4>
                    <button data-toggle="modal" data-target="#myModal" class="pull-right md-trigger m-b-5 m-r-2 btn btn-info btn-sm fa fa-plus-circle"> Upload CSV Import List</button>

                </div>
            </div>
            <div class="panel-body">
                <p class="m-b-15">Available Data
                	<a href="{{ url('kra-pin/import')}}" class="btn btn-sm btn-success fa fa-search pull-right"> Search </a>
                </p>
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
                        <thead>
                        	<th>View</th>
							<th>Kra Pin</th>
							<th>L. Name</th>
							<th>F. Name</th>
							<th>M. Name</th>
							<th>M PO BOX</th>
							<th>Town</th>
							<th>P Code</th>
							<th>R Tel No</th>
							<th>Id Type</th>
							<th>ID no</th>
							<th>STARTUPDATE</th>
						</thead>
						<tbody>
						@foreach($kra_data as $data)
						<tr>
							<td>
								<a href="{{ url('bio-data',$data->bio_data_id) }}" class="label label-success"> View Details
								</a>
							</td>
							<td>{{ $data->TAXPAYERPIN }} </td>
							<td>{{ $data->LASTNAME }} </td>
							<td>{{ $data->FIRSTNAME }} </td>
							<td>{{ $data->MIDENAME }} </td>
							<td>{{ $data->MPOBOX }} </td>
							<td>{{ $data->MTOWN }} </td>
							<td>{{ $data->POCODE }} </td>
							<td>{{ $data->RTELNO }} </td>
							<td>{{ $data->IDTYPE }} </td>
							<td>{{ $data->IDNUMBER }} </td>
							<td>{{ $data->STARTUPDATE }} </td>
						</tr>
						@endforeach
						</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">CSV Import List</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="{{ url('kar_csv_import')}}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label for="exampleName">CSV File  format Only </label>
							<input type="file" class="form-control" name="file" accept=".csv">
						</div>
						<button type="submit" class="btn btn-base btn-sm pull-right"><i class="fa fa-plus-circle"></i> Import  </button>
					</form>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

</div>
@endsection