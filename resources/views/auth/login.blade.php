@extends('layouts.maincontent')
@section('content')
        
        <!-- Content Wrapper -->
        <div class="login-wrapper">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-bd">
                    <div class="panel-heading">
                        <div class="view-header">
                            <div class="header-icon">
                                <i class="pe-7s-unlock"></i>
                            </div>
                            <div class="header-title">
                                <small><strong>Please enter your credentials to login.</strong></small>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label">Username</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="email" type="text" placeholder="username" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus>
                                </div>
                                <span class="help-block small">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="******">
                                </div>
                                <span class="help-block small">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </span>
                            </div>
                            <div>
                                <div class="checkbox checkbox-success">
                                    <input id="checkbox3" type="checkbox"  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="checkbox3">Keep me signed in</label>
                                </div>
                                <button type="submit" class="btn btn-block btn-info" style="background:#ba0c2f;border:1px solid #ba0c2f;"> Login</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>  <!-- /.content-wrapper -->
@endsection
