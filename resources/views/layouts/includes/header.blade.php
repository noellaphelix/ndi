<a href="{{ url('dashboard')}}" class="logo"> <!-- Logo -->
	<span class="logo-mini">
		<img src="{{ asset('jubilee-logo.svg') }}"  style="padding-top:5px !important;" width="60%" >
	</span>
	<span class="logo-lg">
		<img src="{{ asset('jubilee-logo.svg') }}"  style="padding-top:5px !important;" width="60%" >
	</span>
</a>
<!-- Header Navbar -->
<nav class="navbar navbar-static-top">
	<a href="#" class="sidebar-toggle hidden-sm hidden-md hidden-lg" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
		<span class="sr-only">Toggle navigation</span>
		<span class="ti-menu-alt"></span> 
	</a>
	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">

			<li class="dropdown dropdown-user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ti-user"></i></a>
				<ul class="dropdown-menu">
					<li><a href="profile.html"><i class="ti-user"></i> User Profile</a></li>
					<li><a href="{{ url('config')}}"><i class="ti-settings"></i> Settings</a></li>
					<li><a href="{{ url('logout') }}"><i class="ti-key"></i> Logout</a></li>
				</ul>
			</li>
		</ul>
	</div>
</nav>