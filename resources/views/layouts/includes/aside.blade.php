
<aside class="main-sidebar">
	<!-- sidebar -->
	<div class="sidebar">
		<!-- sidebar menu -->
		<ul class="sidebar-menu">
			<li class="treeview">
				<a href="{{ url('dashboard') }}">
					<i class="ti-home"></i><span>Dashboard</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
			</li>

			<li><a href="{{ url('dl') }}"><i class="fa fa-book"></i>Driving License</a></li>
			<li><a href="{{ url('bio-data') }}"><i class="fa fa-users"></i>Bio - Data</a></li>
			<li><a href="{{ url('kra-pin') }}"><i class="fa fa-users"></i>KRA-verifications</a></li>
			<li><a href="{{ url('vehicle-detial') }}"><i class="fa fa-truck"></i>Vehicle - Details</a></li>
			<li><a href="{{ url('config')}}"><i class="fa fa-cog"></i>Config</a></li>

		</ul>
	</div> <!-- /.sidebar -->
</aside>