@include('layouts.includes.styles')
<body class="hold-transition fixed sidebar-mini">
	<!-- Preloader -->
    <div class="preloader"></div>
    
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">        	
			@include('layouts.includes.header')	
			@include('layouts.includes.aside')	
        </header>

	    <!-- Content Wrapper. Contains page content -->
	    <div class="content-wrapper">
	        <!-- Main content -->
	        <div class="content">            
				@yield('content')
	        </div>
	    </div>
		@include('layouts.includes.footer')
    </div>
	@include('layouts.includes.scripts')
	@yield('scripts')
</body>
