@extends('layouts.maincontent')
@section('content')
<div class="row">
	<div class="col-sm-6 lobipanel-parent-sortable ui-sortable">
		<div class="panel panel-default">
			<div class="panel-heading">
				Config 
				<a href="{{ url('config/token-refresh')}}">Refresh Access Token</a>
			</div>
			<div class="panel-body table-responsive">
				@if($config)
				<table class="table">
					<tr>
						<td> Username </td>
						<td> {{ $config->username }}</td>
					</tr>
					<tr>
						<td> Grant Type </td>
						<td> {{ $config->password }}</td>
					</tr>
					<tr>
						<td> Username </td>
						<td> {{ $config->username }}</td>
					</tr>
					<tr>
						<td> Password </td>
						<td> {{ str_repeat("X",strlen($config->password))}}</td>
					</tr>
					<tr>
						<td> Client Secret </td>
						<td> {{ $config->client_secret }}</td>
					</tr>
					<tr>
						<td> Token </td>
						<td> {{ $config->token }}</td>
					</tr>
					<tr>
						<td> Client Id </td>
						<td> {{ $config->client_id }}</td>
					</tr>
				</table>
				@endif
				
			</div>
		</div>
	</div>
	<div class="col-sm-6 lobipanel-parent-sortable ui-sortable">
	<div class="panel panel-default">
		<div class="panel-heading">
			Create Config 
		</div>
		<div class="panel-body">

					@if($config)
					<form method="POST" action="{{ url('config',$config->id) }}">
					@method('PUT')
					@else
					<form method="POST" action="{{ url('config') }}">
					@endif

					@csrf
					<div class="form-group">
						<label> Grant type </label>
						<input type="text" name="grant_type" class="form-control" value="password">
					</div>
					<div class="form-group">
						<label>Password </label>
						<input type="text" name="password" class="form-control"  >
					</div>
					<div class="form-group">
						<label> Client ID </label>
						<input type="text" name="client_id" class="form-control" value="NSxhDkbIEfNWtv0utD8lqZB7J6l5m91kHYdq6v2m">
					</div>
					<div class="form-group">
						<label> Client Secret </label>
						<input type="text" name="client_secret" class="form-control" value="MdARQCfaOQ4Zk7zqqEIGgY7KVZiY148IqdpvJuD4d3jLKHZYkARKUpfk85mL2BlwO7Jf3nArzRHcwywssBG3JY7Vqd6YB0WVcza4KEYt134RJ4aYX3DEsaUmOI27fVZF">
					</div>
					<div class="form-group">
						<label> UserName </label>
						<input type="text" name="username" class="form-control" value="support@jubileekenya.com">
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-success btn-block btn-sm">{{ $type =$config?" Update Config ":"Create new config" }}</button>
					</div>
				</form>
		</div>
	</div>
	</div>

</div>
@endsection