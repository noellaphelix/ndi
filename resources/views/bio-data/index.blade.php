@extends('layouts.maincontent')
@section('content')
<div class="row">
	<div class="col-md-12">
		@if(session('message'))
		<div class="alert alert-success">
			{{session('message')}}
		</div>
		@endif
	</div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Bio Data</h4>
                     <button data-toggle="modal" data-target="#myModal" class="pull-right md-trigger m-b-5 m-r-2 btn btn-info btn-sm fa fa-plus-circle"> Upload CSV Import List</button>
                </div>
            </div>
            <div class="panel-body">
                <p class="m-b-15">Available Data
                	<a href="{{ url('bio-data/import')}}" class="btn btn-sm btn-success fa fa-search pull-right"> Search </a>
                </p>
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
                        <thead>
							<th>#</th>
							<th>Id Number</th>
							<th>Serial Number</th>
							<th>Date Of issue</th>
							<th>Full names</th>
							<th>Gender</th>
							<th>DOB</th>
							<th>District of Birth</th>
							<th>Fathers name</th>
							<th>Mothers Name</th>
							<th>Adress</th>
						</thead>
						<tbody>
						@foreach($bios_data as $bio_data)
						<tr>
							<td><a href="{{ url('bio-data/'.$bio_data->id) }}" class="fa fa-eye"></a> </td>
							<td>{{ $bio_data->ID_NUMBER }} </td>
							<td>{{ $bio_data->SERIAL_NUMBER }} </td>
							<td>{{ $bio_data->DATE_OF_ISSUE }} </td>
							<td>{{ $bio_data->FULL_NAMES }} </td>
							<td>{{ $bio_data->GENDER }} </td>
							<td>{{ $bio_data->DATE_OF_BIRTH }} </td>
							<td>{{ $bio_data->DISTRICT_OF_BIRTH }} </td>
							<td>{{ $bio_data->FATHER_NAMES }} </td>
							<td>{{ $bio_data->MOTHER_NAMES }} </td>
							<td>{{ $bio_data->ADDRESS }} </td>
						</tr>
						@endforeach
						</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">CSV Import List</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="{{ url('bio_data_csv')}}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label for="exampleName">CSV File format only </label>
							<input type="file" class="form-control" name="file" accept=".csv">
						</div>
						<button type="submit" class="btn btn-base btn-sm pull-right"><i class="fa fa-plus-circle"></i> Import  </button>
					</form>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
</div>
@endsection