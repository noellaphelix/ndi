@extends('layouts.maincontent')
@section('content')
<div class="row">
	<div class="col-md-6 ">
		<form method="POST" action="{{ url('bio-data') }}">
			@csrf
			<div class="form-group">
				<label>Enter ID(s) to Verify</label>
				<textarea name="tofind" class="form-control" rows="4"></textarea>
			</div>
			<div class="form-group">
				<button  type="submit" class="btn btn-info btn-block fa fa-send"> get</button>
			</div>
		</form>
	</div>
	<div class="col-md-6">
		@if(session('searchResults'))
		@php($notfound = session('searchResults'))
	    <div class="panel panel-bd lobidrag">
	        <div class="panel-heading">
	            <div class="panel-title">
	                <h4 class="text-danger">No Result Found For</h4>
	            </div>
            </div>
           <div class="panel-body">
			@foreach($notfound['not_found'] as $key => $value)
				<span class="label label-warning">{{ $value }}</span>
				
			@endforeach
			</div>
		</div>
		@endif
	</div>

</div>

<div class="row">
		    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>BIO Data</h4>
                </div>
            </div>
            <div class="panel-body">
                <p class="m-b-15">Found Results
                	<a href="{{ url('bio-data/import')}}" class="btn btn-sm btn-success fa fa-search pull-right"> Search </a>
                </p>
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
                        <thead>
							<th>Id Number</th>
							<th>Serial Number</th>
							<th>Date Of issue</th>
							<th>Full names</th>
							<th>Gender</th>
							<th>DOB</th>
							<th>District of Birth</th>
							<th>Fathers name</th>
							<th>Mothers Name</th>
							<th>Adress</th>
						</thead>
						<tbody>
						@if(session('data'))
							@php($data = session('data'))
							@foreach($data as $bio_data)
							<tr>
								<td>{{ $bio_data->ID_NUMBER }} </td>
								<td>{{ $bio_data->SERIAL_NUMBER }} </td>
								<td>{{ $bio_data->DATE_OF_ISSUE }} </td>
								<td>{{ $bio_data->FULL_NAMES }} </td>
								<td>{{ $bio_data->GENDER }} </td>
								<td>{{ $bio_data->DATE_OF_BIRTH }} </td>
								<td>{{ $bio_data->DISTRICT_OF_BIRTH }} </td>
								<td>{{ $bio_data->FATHER_NAMES }} </td>
								<td>{{ $bio_data->MOTHER_NAMES }} </td>
								<td>{{ $bio_data->ADDRESS }} </td>
							</tr>
							@endforeach
						@endif

						@if(session('searchResults'))
							@php($data = session('searchResults'))
							@foreach($data['found'] as $key => $bio_data)
								<tr>
									<td>{{ $bio_data[0]->ID_NUMBER }} </td>
									<td>{{ $bio_data[0]->SERIAL_NUMBER }} </td>
									<td>{{ $bio_data[0]->DATE_OF_ISSUE }} </td>
									<td>{{ $bio_data[0]->FULL_NAMES }} </td>
									<td>{{ $bio_data[0]->GENDER }} </td>
									<td>{{ $bio_data[0]->DATE_OF_BIRTH }} </td>
									<td>{{ $bio_data[0]->DISTRICT_OF_BIRTH }} </td>
									<td>{{ $bio_data[0]->FATHER_NAMES }} </td>
									<td>{{ $bio_data[0]->MOTHER_NAMES }} </td>
									<td>{{ $bio_data[0]->ADDRESS }} </td>
								</tr>
							@endforeach
						@endif

						</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection