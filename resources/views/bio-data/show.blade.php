@extends('layouts.maincontent')
@section('content')
<div class="row">
    <div class="col-sm-12">
    <div class="panel panel-bd lobidrag">
        <div class="panel-heading">
                <div class="panel-title">
                    <a href="{{ URL::Previous() }}" class="btn btn-sm btn-info">Back</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
  <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Bio Data Details</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table   class="table  table-striped table-hover">
                            <tr>
                                <td>Id Number</td><td> {{ $data->ID_NUMBER }} </td>
                            </tr>
                            <tr>
                                <td>Serial Number</td><td> {{ $data->SERIAL_NUMBER }} </td>
                            </tr>
                            <tr>
                                <td>Date of Issue</td><td> {{ $data->DATE_OF_ISSUE }} </td>
                            </tr>
                             <tr>
                                <td>Names</td><td> {{ $data->FULL_NAMES }} </td>
                            </tr>
                            <tr>
                                <td>Gender</td><td> {{ $data->GENDER }} </td>
                            </tr>
                            @if($data->GENDER != 'COMPANY')
                            <tr>
                                <td>DOB</td><td> {{ $data->DATE_OF_BIRTH }} </td>
                            </tr>
                            <tr>
                                <td>district</td><td> {{ $data->DISTRICT_OF_BIRTH }} </td>
                            </tr>
                            <tr>
                                <td>Fathers Names</td><td> {{ $data->FATHER_NAMES }} </td>
                             </tr>
                             <tr>
                                <td>Mothers Names</td><td> {{ $data->MOTHER_NAMES }} </td>
                            </tr>
                             <tr>
                                <td>Address</td><td> {{ $data->ADDRESS }} </td>
                            </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4> KRa Details</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        @if(!$data->kra == null)
                        <table   class="table  table-striped table-hover">
                            <tr>
                                <td>Kra Pin</td><td> {{ $data->kra->TAXPAYERPIN }} </td>
                            </tr>
                            <tr>
                                <td>Last Name</td><td> {{ $data->kra->LASTNAME }} </td>
                            </tr>
                            <tr>
                                <td>First Name</td><td> {{ $data->kra->FIRSTNAME }} </td>
                            </tr>
                             <tr>
                                <td>Middle Name</td><td> {{ $data->kra->MIDDLENAME }} </td>
                            </tr>
                            <tr>
                                <td>P.O.Box</td><td> {{ $data->kra->MPOBOX }} </td>
                            </tr>
                            <tr>
                                <td>Town</td><td> {{ $data->kra->MTOWN }} </td>
                            </tr>
                            <tr>
                                <td>Pcode</td><td> {{ $data->kra->POCODE }} </td>
                            </tr>
                            <tr>
                                <td>Rtel No</td><td> {{ $data->kra->RTELNO }} </td>
                             </tr>
                             <tr>
                                <td>Id Type</td><td> {{ $data->kra->IDTYPE }} </td>
                            </tr>
                             <tr>
                                <td>update</td><td> {{ $data->kra->STARTUPDATE }} </td>
                            </tr>

                        </table>
                        @else
                        <div  class="alert alert-warning">
                            There is no KRA DATA LISTED YET (Request KRA PIN Or Vehicle Registration from client)
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="row">
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Vehicle Listed</h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    @if(!$data->kra == null)
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
                        <thead>
                            <th>Listed Names</th>
                            <th>Chasis Number</th>
                            <th>Manu (yr)</th>
                            <th>Make</th>
                            <th>Model</th>
                            <th>Reg</th>
                            <th>Body T</th>
                            <th>Log Bk</th>
                            <th>Reg Date</th>
                            <th>Body C</th>
                            <th>Duty Status</th>
                            <th>Duty Amount</th>
                            <th>Duty Date</th>
                            <th>Engine Number</th>
                            <th>Caveat</th>
                        </thead>
                        <tbody>
                        @foreach($data->kra->vehicles as $vehicle)
                        <tr>
                            <td>
                                @foreach($vehicle->krapin as $owner)

                                @if($owner->bio_data_id  != $data->id)
                                 <a href="{{ url('bio-data',$owner->bio_data_id) }}" class="label label-success">
                                    {{ $owner->LASTNAME }}
                                    {{ $owner->FIRSTNAME }}
                                    {{ $owner->MIDDLENAME }}
                                     </a>
                                @endif
                                @endforeach
                            </td>
                            <td>{{ $vehicle->chassisNumber }} </td>
                            <td>{{ $vehicle->yearOfManufacture }} </td>
                            <td>{{ $vehicle->carMake }} </td>
                            <td>{{ $vehicle->carModel }} </td>
                            <td>{{ $vehicle->regNo }} </td>
                            <td>{{ $vehicle->bodyType }} </td>
                            <td>{{ $vehicle->logbookNumber }} </td>
                            <td>{{ $vehicle->registrationDate }} </td>
                            <td>{{ $vehicle->bodyColor }} </td>
                            <td>{{ $vehicle->dutyStatus }} </td>
                            <td>{{ $vehicle->dutyAmount }} </td>
                            <td>{{ $vehicle->dutyDate }} </td>
                            <td>{{ $vehicle->engineNumber }} </td>
                            <td>{{ $vehicle->caveat }} </td>
                        </tr>
                        @endforeach
                        </tbody>
                </table>
                @else
                <div  class="alert alert-warning">
                    No data Listed Yet , Request Vehicle Registration from client
                </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
    </div>


@endsection

@section('scripts')

@endsection