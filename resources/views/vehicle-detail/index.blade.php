@extends('layouts.maincontent')
@section('content')
<div class="row">
	<div class="col-md-12">
		@if(session('message'))
		<div class="alert alert-success">
			{{session('message')}}
		</div>
		@endif
	</div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Vehicle Details</h4>

                    <button data-toggle="modal" data-target="#myModal" class="pull-right md-trigger m-b-5 m-r-2 btn btn-info btn-sm fa fa-plus-circle"> Upload CSV Import List</button>
                </div>
            </div>
            <div class="panel-body">
                <p class="m-b-15">Available Data
                	<a href="{{ url('vehicle-detial/import')}}" class="btn btn-sm btn-success fa fa-search pull-right"> Search </a>
                </p>
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
                        <thead>
							<th>View</th>
							<th>Chasis Number</th>
							<th>Manu (yr)</th>
							<th>Make</th>
							<th>Model</th>
							<th>Reg</th>
							<th>Body T</th>
							<th>Log Bk</th>
							<th>Reg Date</th>
							<th>Body C</th>
							<th>Duty Status</th>
							<th>Duty Amount</th>
							<th>Duty Date</th>
							<th>Engine Number</th>
							<th>Caveat</th>
						</thead>
						<tbody>
						@foreach($vdetails as $vehicle)
						<tr>
							<td><a href="{{ url('vehicle-detial',$vehicle->id) }}" class="fa fa-eye"> View</a></td>
							<td>{{ $vehicle->chassisNumber }} </td>
							<td>{{ $vehicle->yearOfManufacture }} </td>
							<td>{{ $vehicle->carMake }} </td>
							<td>{{ $vehicle->carModel }} </td>
							<td>{{ $vehicle->regNo }} </td>
							<td>{{ $vehicle->bodyType }} </td>
							<td>{{ $vehicle->logbookNumber }} </td>
							<td>{{ $vehicle->registrationDate }} </td>
							<td>{{ $vehicle->bodyColor }} </td>
							<td>{{ $vehicle->dutyStatus }} </td>
							<td>{{ $vehicle->dutyAmount }} </td>
							<td>{{ $vehicle->dutyDate }} </td>
							<td>{{ $vehicle->engineNumber }} </td>
							<td>{{ $vehicle->caveat }} </td>
						</tr>
						@endforeach
						</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">CSV Import List</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="{{ url('vehicle_csv')}}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label for="exampleName">Csv File Format Only </label>
							<input type="file" class="form-control" name="file" accept=".csv">
						</div>
						<button type="submit" class="btn btn-base btn-sm pull-right"><i class="fa fa-plus-circle"></i> Import  </button>
					</form>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

</div>
@endsection