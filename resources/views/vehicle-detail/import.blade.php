@extends('layouts.maincontent')
@section('content')
<div class="row">
	<div class="col-md-6 ">
		<form method="POST" action="{{ url('vehicle-detial') }}">
			@csrf
			<div class="form-group">
				<label>Enter Vehicle Registration number to Verify</label>
				<textarea name="tofind" class="form-control" rows="4"></textarea>
			</div>
			<div class="form-group">
				<button  type="submit" class="btn btn-info btn-block fa fa-send"> get</button>
			</div>
		</form>
	</div>
	<div class="col-md-6">
		@if(session('searchResults'))
		@php($notfound = session('searchResults'))
	    <div class="panel panel-bd lobidrag">
	        <div class="panel-heading">
	            <div class="panel-title">
	                <h4 class="text-danger">No Result Found For</h4>
	            </div>
            </div>
           <div class="panel-body">
			@foreach($notfound['not_found'] as $key => $value)
				<span class="label label-warning">{{ $value }}</span>
				
			@endforeach
			</div>
		</div>
		@endif
	</div>

</div>

<div class="row">
		    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Vehicle Data</h4>
                </div>
            </div>
            <div class="panel-body">
                <p class="m-b-15">Found Results
                	<a href="{{ url('bio-data/import')}}" class="btn btn-sm btn-success fa fa-search pull-right"> Search </a>
                </p>
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
   						<thead>
							<th>Chasis Number</th>
							<th>Manu (yr)</th>
							<th>Make</th>
							<th>Model</th>
							<th>Reg</th>
							<th>Body T</th>
							<th>Log Bk</th>
							<th>Reg Date</th>
							<th>Body C</th>
							<th>Duty Status</th>
							<th>Duty Amount</th>
							<th>Duty Date</th>
							<th>Engine Number</th>
							<th>Caveat</th>
						</thead>
						<tbody>
						@if(session('data'))
							@php($data = session('data'))
							@foreach($data as $vehicle)
						<tr>
							<td>{{ $vehicle->chassisNumber }} </td>
							<td>{{ $vehicle->yearOfManufacture }} </td>
							<td>{{ $vehicle->carMake }} </td>
							<td>{{ $vehicle->carModel }} </td>
							<td>{{ $vehicle->regNo }} </td>
							<td>{{ $vehicle->bodyType }} </td>
							<td>{{ $vehicle->logbookNumber }} </td>
							<td>{{ $vehicle->registrationDate }} </td>
							<td>{{ $vehicle->bodyColor }} </td>
							<td>{{ $vehicle->dutyStatus }} </td>
							<td>{{ $vehicle->dutyAmount }} </td>
							<td>{{ $vehicle->dutyDate }} </td>
							<td>{{ $vehicle->engineNumber }} </td>
							<td>{{ $vehicle->caveat }} </td>
						</tr>
							@endforeach
						@endif

						@if(session('searchResults'))
							@php($data = session('searchResults'))
							@foreach($data['found'] as $key => $veh)
						<tr>
							<td>{{ $veh->chassisNumber }}</td>
							<td>{{ $veh->vehicle->yearOfManufacture }}</td>
							<td>{{ $veh->vehicle->carMake }}</td>
							<td>{{ $veh->vehicle->carModel }}</td>
							<td>{{ $veh->vehicle->regNo }}</td>
							<td>{{ $veh->vehicle->bodyType }}</td>
							<td>{{ $veh->vehicle->logbookNumber }}</td>
							<td>{{ $veh->vehicle->registrationDate }}</td>
							<td>{{ $veh->vehicle->bodyColor }}</td>
							<td>{{ $veh->vehicle->dutyStatus }}</td>
							<td>{{ $veh->vehicle->dutyAmount }}</td>
							<td>{{ $veh->vehicle->dutyDate }}</td>
							<td>{{ $veh->vehicle->engineNumber }}</td>
							<td>{{ $veh->caveat }}</td>
						</tr>
							@endforeach
						@endif

						</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection