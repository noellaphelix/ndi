@extends('layouts.maincontent')
@section('content')
<div class="row">
    <div class="col-sm-12">
    <div class="panel panel-bd lobidrag">
        <div class="panel-heading">
                <div class="panel-title">
                    <a href="{{ URL::Previous() }}" class="btn btn-sm btn-info">Back</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
  <div class="row">
        <div class="col-sm-8">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Listed Owner(s)</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">

                        <thead>
                            <th>Detail</th>
                            <th>Serial Number</th>
                            <th>Date of Issue</th>
                            <th>Names</th>
                            <th>Gender</th>
                            <th>DOB</th>
                        </thead>
                        <tbody>
                        @foreach($data->krapin as $owner)
                        <tr>
                            <td> <a href="{{ url('bio-data',$owner->id)}}"><label class="label label-success">Details</label></a> </td>
                            <td>{{ $owner->biodata->SERIAL_NUMBER }} </td>
                            <td>{{ $owner->biodata->DATE_OF_ISSUE }} </td>
                            <td>{{ $owner->biodata->FULL_NAMES }} </td>
                            <td>{{ $owner->biodata->GENDER }} </td>
                            <td>{{ $owner->biodata->DATE_OF_BIRTH }} </td>
                        </tr>
                        @endforeach
                        </tbody>
                     </table>
                    </div>
                </div>
            </div>
        </div>
     
<div class="row">
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Vehicle Details Details</h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
                        <thead>
                            <th>Chasis Number</th>
                            <th>Manu (yr)</th>
                            <th>Make</th>
                            <th>Model</th>
                            <th>Reg</th>
                            <th>Body T</th>
                            <th>Log Bk</th>
                            <th>Reg Date</th>
                            <th>Body C</th>
                            <th>Duty Status</th>
                            <th>Duty Amount</th>
                            <th>Duty Date</th>
                            <th>Engine Number</th>
                            <th>Caveat</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $data->chassisNumber }} </td>
                            <td>{{ $data->yearOfManufacture }} </td>
                            <td>{{ $data->carMake }} </td>
                            <td>{{ $data->carModel }} </td>
                            <td>{{ $data->regNo }} </td>
                            <td>{{ $data->bodyType }} </td>
                            <td>{{ $data->logbookNumber }} </td>
                            <td>{{ $data->registrationDate }} </td>
                            <td>{{ $data->bodyColor }} </td>
                            <td>{{ $data->dutyStatus }} </td>
                            <td>{{ $data->dutyAmount }} </td>
                            <td>{{ $data->dutyDate }} </td>
                            <td>{{ $data->engineNumber }} </td>
                            <td>{{ $data->caveat }} </td>
                        </tr>
                        </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>


@endsection

@section('scripts')

@endsection