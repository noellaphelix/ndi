@extends('layouts.maincontent')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Driving Licenses</h4>
                </div>
            </div>
            <div class="panel-body">
                <p class="m-b-15">Available Data
                	<a href="{{ url('dl/import')}}" class="btn btn-sm btn-success fa fa-search pull-right"> Search </a>
                </p>
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
                        <thead>
							<th>DLNO</th>
							<th>NAMES</th>
							<th>IDNO</th>
							<th>PIN</th>
							<th>VALIDDATE</th>
							<th>DLCLASS</th>
							<th>SMARTDL</th>
						</thead>
						<tbody>
						@foreach($dls as $dl)
						<tr>
							<td>{{ $dl->DLNO }} </td>
							<td>{{ $dl->NAMES }} </td>
							<td>{{ $dl->IDNO }} </td>
							<td>{{ $dl->PIN }} </td>
							<td>{{ $dl->VALIDDATE }} </td>
							<td>{{ $dl->DLCLASS }} </td>
							<td>{{ $dl->SMARTDL }} </td>
						</tr>
						@endforeach
						</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection