@extends('layouts.maincontent')
@section('content')
<div class="row">
	<div class="col-md-6 ">
		<form method="POST" action="{{ url('dl') }}">
			@csrf
			<div class="form-group">
				<label>Enter DL  to Verify</label>
				<textarea name="tofind" class="form-control" rows="4"></textarea>
			</div>
			<div class="form-group">
				<button  type="submit" class="btn btn-info btn-block fa fa-send"> get</button>
			</div>
		</form>
	</div>
	<div class="col-md-6">
		@if(session('searchResults'))
		@php($notfound = session('searchResults'))
	    <div class="panel panel-bd lobidrag">
	        <div class="panel-heading">
	            <div class="panel-title">
	                <h4 class="text-danger">No Result Found For</h4>
	            </div>
            </div>
           <div class="panel-body">
			@foreach($notfound['not_found'] as $key => $value)
				<span class="label label-warning">{{ $value }}</span>
				
			@endforeach
			</div>
		</div>
		@endif
	</div>

</div>

<div class="row">
		    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Dl  Data</h4>
                </div>
            </div>
            <div class="panel-body">
                <p class="m-b-15">Found Results
                	<a href="{{ url('bio-data/import')}}" class="btn btn-sm btn-success fa fa-search pull-right"> Search </a>
                </p>
                <div class="table-responsive">
                    <table  id="dataTableExample1" class="table table-bordered table-striped table-hover datatables">
                        <thead>
							<th>DLNO</th>
							<th>NAMES</th>
							<th>IDNO</th>
							<th>PIN</th>
							<th>VALIDDATE</th>
							<th>DLCLASS</th>
							<th>SMARTDL</th>
						</thead>
						<tbody>
						@if(session('data'))
							@php($data = session('data'))
							@foreach($data as $dl)
							<tr>
								<td>{{ $dl->DLNO }} </td>
								<td>{{ $dl->NAMES }} </td>
								<td>{{ $dl->IDNO }} </td>
								<td>{{ $dl->PIN }} </td>
								<td>{{ $dl->VALIDDATE }} </td>
								<td>{{ $dl->DLCLASS }} </td>
								<td>{{ $dl->SMARTDL }} </td>
							</tr>
							@endforeach
						@endif

						@if(session('searchResults'))
							@php($data = session('searchResults'))
							@foreach($data['found'] as $key => $dl)
								<tr>
								<td>{{ $dl->DLNO }} </td>
								<td>{{ $dl->NAMES }} </td>
								<td>{{ $dl->IDNO }} </td>
								<td>{{ $dl->PIN }} </td>
								<td>{{ $dl->VALIDDATE }} </td>
								<td>{{ $dl->DLCLASS }} </td>
								<td>{{ $dl->SMARTDL }} </td>
								</tr>
							@endforeach
						@endif

						</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection